$(document).ready(function() {
	$('#tab > a').click(function() {

		var i = $(this).index();

		$(this).siblings().removeClass('current').end().addClass('current');
		$('.tab-box').hide().eq(i).show();
		return false;
	});

	$('#ad-logo marquee').marquee();

	Date.format = 'yyyy-mm-dd';
	$('.date-pick').datePicker({
		autoFocusNextInput: true,
		startDate: '1990-1-1'       
	});

	$('.richtext').each(function(){
		var self = this;
		$(this).wysiwyg({
			autoGrow:true, 
			controls:"bold,italic,underline,|,subscript,superscript,|,insertOrderedList,insertUnorderedList", 
			initialContent: $(self).val()
		});
	});	

	$('.richtext-full-feature').wysiwyg({
		autoGrow:true,
		controls:"bold,italic,underline,|,subscript,superscript,|,insertOrderedList,insertUnorderedList,|,justifyLeft,justifyCenter,justifyRight,justifyFull", 
		initialContent: $('.richtext-full-feature').val()
	});
});