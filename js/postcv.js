$(function(){
	var _fields = {

		// education background
		'edu-bg' : [
			'institute',
			'course',
			'edu-from-year',
			'edu-to-year'
		],

		// other qualification
		'other-q' : [
			'title',
			'other-from-year',
			'other-to-year'
		],

		// employment history
		'emp-his' : [
			'emp-from-year',
			'emp-to-year',
			'post',
			'company',
			'salary'
		]
	};

	$('.delete').click(function(){
			
		$(this).parents( 'tr' ).fadeOut('slow', function(){
			$(this).remove();
		});

		return false;		
	})

	$( "#edu-bg-add, #other-q-add, #emp-his-add" ).click(function(){

		var grid_id = _fetch_grid_id($(this).attr( 'id' ));

		var field_list = _fields[ grid_id ];

		$(_compose_row( grid_id )).appendTo( '#' + grid_id ).find( '.delete' ).click(function(){
			
			$(this).parents( 'tr' ).fadeOut('slow', function(){
				$(this).remove();
			});

			return false;
		});
		
		$.each( field_list, function( k, v ){
			$('input[name='+ v +']').val( '' );
		});
	});

	function _fetch_grid_id( button_id ) {
		if ( button_id.constructor != String ) {
			return false;
		};

		var token = button_id.split( '-' );

		return token[ 0 ] + '-' + token[ 1 ];
	}


	function _compose_row( type ) {

		var html = '';

		if (type == 'edu-bg') {

			$([ _grid_var('institute'),
			    _grid_var('course'),
			    _grid_var('edu-from-year') + ' - '
			   + _grid_var('edu-to-year') ]).each(function(k,v){
			   		html += '<td>' + v + '</td>';
			   });
		}

		else if (type == 'other-q') {
			$([ _grid_var( 'other-course' ),
			    _grid_var( 'other-from-year' ) + ' - '
			   + _grid_var( 'other-to-year' ) ]).each(function(k,v){
			   		html += '<td>' + v + '</td>';
			   });
		}

		else if (type == 'emp-his') {
			$([  _grid_var( 'emp-from-year' ) + ' - '
			   + _grid_var( 'emp-to-year' ) ,
			   _grid_var( 'post' ),
			   _grid_var( 'company' ),
			   _grid_var( 'salary' ) ]).each(function(k,v){
			   		html += '<td>' + v + '</td>';
			   });
		}

		// html += '<td><a href="#" class="edit">Edit</a> | <a href="#" class="delete">Delete</a></td>';
		html += '<td><input type=button class=delete></td>';
		return '<tr>' + html + '</tr>';
	}

	/*$( 'a.edit' ).live('click',function(e){

	});*/

	/*$( 'a.delete' ).live('click',function(){
		$(this).parents('tr').hide('fast',function(){
			$(this).remove();
		})
	});	*/

	function _grid_var( name ) {
		var value = _get_value_by_tag_name( name );
		var hidden = _html_input_hidden( name, value );

		return '<span>' + value + '</span>' + hidden;
	}

	function _html_input_hidden( name, value ) {
		return '<input type="hidden" name="' + name + '[]" value="' + value + '">';
	}

	function _get_value_by_tag_name( input_tag_name )
	{
		return document.getElementsByName( input_tag_name )[0].value;
	}
});